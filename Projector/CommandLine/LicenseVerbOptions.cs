﻿// (C) WithLithum 2022.
// Licensed under GPL-3.0-or-later license.

namespace Projector.CommandLine;

using global::CommandLine;

[Verb("license", HelpText = "Gets a license.")]
internal class LicenseVerbOptions
{
    [Option('p', "print-info", Default = true, Required = false,
        HelpText = "Whether to print the info of the license to the console.")]
    public bool PrintInfo { get; set; }

    [Option('o', "output", Default = "none", Required = false,
        HelpText = "Output of the license text. If set to none, no license text is written.")]
    public string Output { get; set; } = "none";

    [Value(0, Required = true,
        HelpText = "License name.")]
    public string? Name { get; set; }
}
