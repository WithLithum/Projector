﻿// (C) WithLithum 2022.
// Licensed under GPL-3.0-or-later license.

namespace Projector.CommandLine;

using Projector.Util;
using System;
using System.Text;

internal static class Executor
{
    internal static int LicenseVerb(LicenseVerbOptions opts)
    {
        // Initializes license dictionary
        LicenseListUtil.ParseLicenseData();

        if (opts.Name == null)
        {
            Console.WriteLine("Error: Empty license name");
            return 0x02;
        }

        if (!LicenseListUtil.LicenseInformation.ContainsKey(opts.Name))
        {
            Console.WriteLine("Error: No such license");
            return 0x01;
        }


        var context = LicenseListUtil.GetContext(LicenseListUtil.LicenseInformation[opts.Name]);

        if (context == null)
        {
            Console.WriteLine("Failed to get context");
            return 0x03;
        }

        // If output was specified, outputs to the specified file.
        if (opts.Output != "none")
        {
#if DEBUG
            Console.WriteLine(context.LicenseText.Length);
            Console.WriteLine(context.Name);
#endif

            try
            {
                File.WriteAllText(opts.Output, context.LicenseText);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to create file:");
                Console.WriteLine(ex);
            }
        }

        // If print info was specified, prints relevant information in the console.
        if (opts.PrintInfo)
        {
            var sb = new StringBuilder();
            sb.Append("License Name: ").AppendLine(context.Name);
            sb.Append("ID: ").AppendLine(context.LicenseId);
            sb.Append("Comment: ").AppendLine(context.LicenseComments);
            sb.Append("Listed as free by FSF: ").AppendLine(context.IsFsfLibre ? "Yes" : "No");
            sb.Append("Approved by OSI: ").AppendLine(context.IsOsiApproved ? "Yes" : "No");

            sb.AppendLine("Cross References: ");
            foreach (var cref in context.CrossRef)
            {
                sb.Append("    ").AppendLine(cref.Uri);
            }

            sb.AppendLine("See also: ");
            foreach (var uri in context.SeeAlso)
            {
                sb.Append("    ").AppendLine(uri);
            }
        }

        return 0;
    }

    internal static int ProjectVerb(ProjectVerbOptions opts)
    {
        return 0;
    }
}
