﻿// (C) WithLithum 2022.
// Licensed under GPL-3.0-or-later license.

using CommandLine;
using Projector.CommandLine;
using Projector.Util;

Console.WriteLine("Hello, World!");

try
{
    await LicenseListUtil.CheckAndDownloadLicenseList();
}
catch (TaskCanceledException tce)
{
    Console.WriteLine(tce.Message);
    return -1;
}

return Parser.Default.ParseArguments<LicenseVerbOptions, ProjectVerbOptions>(args)
    .MapResult(
      (LicenseVerbOptions opts) => Executor.LicenseVerb(opts),
      (ProjectVerbOptions opts) => Executor.ProjectVerb(opts),
      errs => 1);