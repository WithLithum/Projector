﻿namespace Projector.Util;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
public record struct CrossReference(
    string Match,
    string Uri,
    bool IsValid,
    bool IsLive,
    DateTime TimeStamp,
    bool IsWayBackLink,
    int Order
);

// HACK this won't read any data!
public class LicenseContext
{

    public bool IsDeprecatedLicenseId { get; set; }

    public List<CrossReference> CrossRef { get; set; } = new();

    [JsonProperty("licenseText")]
    public string LicenseText { get; set; } = "";

    public List<string> SeeAlso { get; set; } = new();

    public string LicenseTextHtml { get; set; } = "";

    public string StandardLicenseHeaderHtml { get; set; } = "";

    public string StandardLicenseHeaderTemplate { get; set; } = "";

    public string StandardLicenseTemplate { get; set; } = "";

    public string Name { get; set; } = "Empty License";

    public string LicenseComments { get; set; } = "If you saw this, it means I somehow messed up";

    public string LicenseId { get; set; } = "";

    public string StandardLicenseHeader { get; set; } = "";

    /// <summary>
    /// Gets or sets a value indicating whether this license is approved by the
    /// Open Source Initiative.
    /// </summary>
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
    public bool IsOsiApproved { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether this license is listed as
    /// a Free Software license on the GNU Project license list page by the
    /// Free Software Foundation.
    /// </summary>
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
    public bool IsFsfLibre { get; set; }
}
