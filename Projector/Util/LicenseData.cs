﻿// (C) WithLithum 2022.
// Licensed under GPL-3.0-or-later license.

namespace Projector.Util;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

[JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
public class LicenseData
{
    public string Reference { get; set; } = "";
    public bool IsDeprecatedLicenseId { get; set; }
    public string DetailsUrl { get; set; } = "";
    public int ReferenceNumber { get; set; }
    public string Name { get; set; } = "license";
    public string LicenseId { get; set; } = "";
    public List<string>? SeeAlso { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether this license is approved by the
    /// Open Source Initiative.
    /// </summary>
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
    public bool IsOsiApproved { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether this license is listed as
    /// a Free Software license on the GNU Project license list page by the
    /// Free Software Foundation.
    /// </summary>
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore, Required = Required.DisallowNull)]
    public bool IsFsfLibre { get; set; }
}
