﻿// (C) WithLithum 2022.
// Licensed under GPL-3.0-or-later license.

namespace Projector.Util;

using Newtonsoft.Json;
using System.Collections.Generic;

public class LicenseList
{
    [JsonProperty("licenseListVersion")]
    public string Version { get; set; } = "";

    [JsonProperty("licenses")]
    public List<LicenseData> LicenseData { get; set; } = new List<LicenseData>();
}
