﻿// (C) WithLithum 2022.
// Licensed under GPL-3.0-or-later license.

namespace Projector.Util;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

internal static class LicenseListUtil
{
    private static readonly string licensePath;
    private static readonly string cachePath;
    private static readonly Dictionary<string, LicenseData> licenseData = new();

    public static readonly ReadOnlyDictionary<string, LicenseData> LicenseInformation = new(licenseData);

#pragma warning disable S3963 // "static" fields should be initialized inline
    static LicenseListUtil()
    {
        var version = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? throw new IOException();

        cachePath = Path.Combine(version, "license-data");
        licensePath = Path.Combine(cachePath, "licenses.json");
    }
#pragma warning restore S3963 // "static" fields should be initialized inline

    internal static void ParseLicenseData()
    {
        var list = JsonConvert.DeserializeObject<LicenseList>(File.ReadAllText(licensePath));

        if (list == null) return;

        // Add all data with license ID as dictionary key
        foreach (var data in list.LicenseData)
        {
            licenseData.Add(data.LicenseId, data);
        }
    }

    internal static LicenseContext? GetContext(LicenseData data)
    {
        if (data == null) throw new ArgumentNullException(nameof(data));

        var detailPath = Path.Combine(cachePath, "details", data.LicenseId + ".json");
        Directory.CreateDirectory(Path.Combine(cachePath, "details"));

        if (!File.Exists(detailPath))
        {
            // Download the file first.

            Console.WriteLine("Downloading license details... this may take some time.");
            var httpClient = new HttpClient();

            using (var reader = httpClient.GetStreamAsync(UrlData.LicenseListUrl).GetAwaiter().GetResult())
            {
                using var fileStream = File.Create(detailPath);
                reader.CopyTo(fileStream);
            }
        }

        // Parse the file.

        var detail = File.ReadAllText(detailPath);

#if DEBUG
        Console.WriteLine(detail[..10]);
#endif

        return JsonConvert.DeserializeObject<LicenseContext>(detail);
    }

    internal static async Task CheckAndDownloadLicenseList()
    {
        // If does not exist, create a new one
        Directory.CreateDirectory(cachePath);


        if (File.Exists(licensePath))
        {
            return;
        }

        var httpClient = new HttpClient();

        using (var reader = await httpClient.GetStreamAsync(UrlData.LicenseListUrl))
        {
            using var fileStream = File.Create(licensePath);
            await reader.CopyToAsync(fileStream);
        }
    }
}
